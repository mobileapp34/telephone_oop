import 'dart:ffi';

import 'package:telephone/telephone.dart' as telephone;

abstract class Telephone {
  var type;
  var color;
  var size;
  var price;

  Telephone(type, color, size, price) {
    this.type = type;
    this.color = color;
    this.size = size;
    this.price = price;
  }
}

class Intro {
  void nexttele() {}
  void backtele() {}
  void recommend() {}
}

class Apple extends Telephone implements Intro {
  Apple(super.type, super.color, super.size, super.price);

  @override
  void backtele() {
    print("For the more than back Future Technology Telephond ${type}");
  }

  @override
  void nexttele() {
    print("For the next Future Technology Telephond ${type}");
  }

  @override
  void recommend() {
    print(
        "This Technology For the Future ${type} and Price don't worry ${price} and more color you need ${color}");
  }
}

class Sumsung extends Telephone implements Intro {
  Sumsung(super.type, super.color, super.size, super.price);

  @override
  void backtele() {
    print("For the more than back Future Technology Telephond ${type}");
  }

  @override
  void nexttele() {
    print("For the next Future Technology Telephond ${type}");
  }

  @override
  void recommend() {
    print(
        "This Technology For the Future ${type} and Price don't worry ${price} and more color you need ${color}");
  }
}
void main(List<String> arguments) {
  var Apple14 = new Apple("Iphone14", "Blue", "Small",  33000);
  Apple14.backtele();
  Apple14.nexttele();
  Apple14.recommend();
  var Sumsung15 = new Sumsung("Galaxy", "Rainbow", "Medium", 29000);
  Sumsung15.backtele();
  Sumsung15.nexttele();
  Sumsung15.recommend();
}